#!/bin/sh

BUILD=$BALLAST_DIR/build/software_build

# Copy the compiled binary
mkdir -p $BUILD
cp target/riscv32imc-unknown-none-elf/release/pac_test $BUILD/test

cd $BUILD

cd $BALLAST_DIR
pulp-run --config-file=pulpissimo@config_file=chips/pulpissimo/pulpissimo_ibex.json --config-opt=platform=rtl --dir=$BALLAST_DIR/build --binary=$BUILD/test prepare


