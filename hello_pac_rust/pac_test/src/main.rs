#![no_std]
#![no_main]

use panic_halt as _;

use sysctrl_rt_sys::printf;
use sysctrl_pac::Peripherals;

#[no_mangle]
fn main() -> ! {
    let peripherals = Peripherals::take().unwrap();
    let core_count = &peripherals.PROCESSOR_INFO_REGISTERS.core_count;

    let val_read = core_count.read();
    let _bits = val_read.core_count().bits();
  

    unsafe {
        printf(b"Hello Rust from PAC test!! I have some peripherals with me\n\0".as_ptr());
        printf(b"Trying to retrieve bits\n\0".as_ptr());
    }

    loop {
    }
}

